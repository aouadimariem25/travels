package com.example.demo.service;

import com.example.demo.entities.Mail;

public interface MailService {
    public void sendEmail(String mailTo,String mailSubject,String mailContent,String mailType);
}