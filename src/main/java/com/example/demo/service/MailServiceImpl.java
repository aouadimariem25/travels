package com.example.demo.service;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.AppUserRepository;
import com.example.demo.dao.VoyageRepository;
import com.example.demo.entities.Mail;

import lombok.RequiredArgsConstructor;

@Transactional
@RequiredArgsConstructor
@Service("mailService")
public class MailServiceImpl implements MailService {
 
    @Autowired
    JavaMailSender mailSender;
    private AppUserRepository appUserRepository;
    public MailServiceImpl(AppUserRepository appUserRepository) {
      
       this.appUserRepository = appUserRepository;
     
    }
    public void sendEmail(String mailTo,String mailSubject,String mailContent,String contentType) {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
 
        try {
        	
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
            
            mimeMessageHelper.setSubject(mailSubject);
            mimeMessageHelper.setFrom("affairev7@gmail.com","affairev7@gmail.com");
            mimeMessageHelper.setTo(mailTo);
            mimeMessageHelper.setText(contentType);
 
            mailSender.send(mimeMessageHelper.getMimeMessage());
 
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
 
}