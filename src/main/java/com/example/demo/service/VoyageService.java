package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import com.example.demo.entities.Voyage;

public interface VoyageService {
	public List<Voyage> getAllVoyages();
    public Voyage findById(int id);
    public void delete(int id);
	public Voyage saveVoyage(Voyage voyage);
	public Voyage editVoyage(int id,Voyage voyage);
	public List<Voyage> findByOD(String objectDeVoyage, String destination);
}
