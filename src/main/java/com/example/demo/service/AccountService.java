package com.example.demo.service;

import java.util.List;

import com.example.demo.entities.AppRole;
import com.example.demo.entities.AppUser;

public interface AccountService {
	public List<AppUser> getAllUsers();
    public AppUser saveUser(String username,String password,String confirmedPassword,String email);
    public AppRole save(AppRole role);
    public AppUser loadUserByUsername(String username);
    public void addRoleToUser(String username,String rolename);
    public AppUser findUserById(int id);
    
}
