package com.example.demo.service;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.AppRoleRepository;
import com.example.demo.dao.AppUserRepository;
import com.example.demo.dao.VoyageRepository;
import com.example.demo.entities.AppUser;
import com.example.demo.entities.Voyage;

import lombok.RequiredArgsConstructor;
import javax.persistence.EntityManager;
@Service
@Transactional
@RequiredArgsConstructor
public class VoyageServiceImpl implements VoyageService{
	@Autowired  
	private VoyageRepository voyageRepository;
	private AppUserRepository appUserRepository;
	 private EntityManager entityManager;
	  public VoyageServiceImpl(VoyageRepository voyageRepository,AppUserRepository appUserRepository,AccountService accountService) {
	        this.voyageRepository = voyageRepository;
	       this.appUserRepository = appUserRepository;
	     
	    }
	@Override
	public List<Voyage> getAllVoyages() {
		List<Voyage> voy=new ArrayList<Voyage>(); 
		voyageRepository.findAll().forEach(voyage -> voy.add(voyage)); 
		System.out.println(voy);
		return voy;
	} 

	
	 @Override
	    public Voyage saveVoyage(Voyage voyageParam) {
	        	System.out.println(voyageParam.getUser().getId());
	        Voyage   voyage=new Voyage();
	        AppUser user=new AppUser();
	        user = appUserRepository.getById(voyageParam.getUser().getId());
	        System.out.println(user);
	        voyage.setDestination(voyageParam.getDestination());
	        voyage.setDateArrivee(voyageParam.getDateArrivee());
	        voyage.setDateDebut(voyageParam.getDateDebut());
	        voyage.setDuree(voyageParam.getDuree());
	        voyage.setObjectDeVoyage(voyageParam.getObjectDeVoyage());
	        voyage.setUser(user);
	        voyageRepository.save(voyage);
	       
	        return voyage;
	    }
	 @Override
	    public Voyage findById(int id) {
	        return voyageRepository.findById(id).get();
	    }
	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		 voyageRepository.deleteById(id);
		 
	}
	@Override
	public Voyage editVoyage(int id,Voyage voyageParam) {
		// TODO Auto-generated method stub
		System.out.println(voyageParam.getUser().getId());
		AppUser user=new AppUser();
        user = appUserRepository.getById(voyageParam.getUser().getId());
       
      Voyage voyage=findById(id);
      voyage.setDestination(voyageParam.getDestination());
      voyage.setDateArrivee(voyageParam.getDateArrivee());
      voyage.setDateDebut(voyageParam.getDateDebut());
      voyage.setDuree(voyageParam.getDuree());
      voyage.setObjectDeVoyage(voyageParam.getObjectDeVoyage());
      voyage.setUser(user);
               voyageRepository.save(voyage);
               return voyage;
       
	}
	@Override
	public List<Voyage> findByOD(String objectDeVoyage, String destination) {
		 List<Voyage> result = voyageRepository.findByObjectDeVoyageAndDestination(objectDeVoyage, destination);
		
		 return result;
	}

	
}
