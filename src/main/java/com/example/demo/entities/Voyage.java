package com.example.demo.entities;

import java.io.Serializable;
import java.util.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;




@Table(name="voyage")
@Entity
@Data @AllArgsConstructor @NoArgsConstructor @ToString
public class Voyage implements Serializable {
	
	private static final long serialVersionUID = 4L;
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	 
	 @Column
	 private String destination;

	 @Column
	 private String objectDeVoyage;
	 
	 @Column
	 private int duree;
	 
	 @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	 @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	 private Date dateDebut;
	 
	 @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	 @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") 
	 private Date dateArrivee;
	 
	
	 @ManyToOne
	 @JoinColumn(name="user", nullable=false)
	 public AppUser user;

	public int getDuree() {
		return duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getObjectDeVoyage() {
		return objectDeVoyage;
	}

	public void setObjectDeVoyage(String objectDeVoyage) {
		this.objectDeVoyage = objectDeVoyage;
	}

	

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateArrivee() {
		return dateArrivee;
	}

	public void setDateArrivee(Date dateArrivee) {
		this.dateArrivee = dateArrivee;
	}

	public Voyage() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateArrivee == null) ? 0 : dateArrivee.hashCode());
		result = prime * result + ((dateDebut == null) ? 0 : dateDebut.hashCode());
		result = prime * result + ((destination == null) ? 0 : destination.hashCode());
		result = prime * result + duree;
		result = prime * result + id;
		result = prime * result + ((objectDeVoyage == null) ? 0 : objectDeVoyage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Voyage other = (Voyage) obj;
		if (dateArrivee == null) {
			if (other.dateArrivee != null)
				return false;
		} else if (!dateArrivee.equals(other.dateArrivee))
			return false;
		if (dateDebut == null) {
			if (other.dateDebut != null)
				return false;
		} else if (!dateDebut.equals(other.dateDebut))
			return false;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		if (duree != other.duree)
			return false;
		if (id != other.id)
			return false;
		if (objectDeVoyage == null) {
			if (other.objectDeVoyage != null)
				return false;
		} else if (!objectDeVoyage.equals(other.objectDeVoyage))
			return false;
		return true;
	}

	

	public AppUser getUser() {
		return user;
	}

	public void setUser(AppUser user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Voyage [id=" + id + ", destination=" + destination + ", objectDeVoyage=" + objectDeVoyage + ", duree="
				+ duree + ", dateDebut=" + dateDebut + ", dateArrivee=" + dateArrivee + "]";
	}

	
	 
	
}
