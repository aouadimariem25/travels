package com.example.demo;

import java.util.stream.Stream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.client.RestTemplate;

import com.example.demo.entities.AppRole;
import com.example.demo.service.AccountService;

@SpringBootApplication
public class TravelsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TravelsApplication.class, args);
	}
	 @Bean
	    CommandLineRunner start(AccountService accountService){
	        return args->{
	            accountService.save(new AppRole(null,"EMPLOYE"));
	            accountService.save(new AppRole(null,"ENTREPRISE"));
	            Stream.of("user1","user2","user3","admin").forEach(un->{
	                accountService.saveUser(un,"1234","1234");
	            });
	            accountService.addRoleToUser("admin","EMPLOYE");
	        };
	    }
	  
	    @Bean
	    BCryptPasswordEncoder getBCPE(){
	        return new BCryptPasswordEncoder();
	    }
	    @Bean
	    public RestTemplate getRestTemplate() {
	    	return new RestTemplate();
	    }
}
