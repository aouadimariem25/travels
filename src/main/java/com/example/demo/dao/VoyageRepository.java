package com.example.demo.dao;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.demo.entities.Voyage;

@RepositoryRestResource
public interface VoyageRepository extends JpaRepository<Voyage,Integer> {
	public	List<Voyage> findByObjectDeVoyageAndDestination(String objectDeVoyage, String destination);
	
}
