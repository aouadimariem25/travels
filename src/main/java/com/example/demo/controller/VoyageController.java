package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.VoyageRepository;
import com.example.demo.entities.Voyage;
import com.example.demo.service.VoyageService;

import lombok.Data;
import net.minidev.json.JSONObject;


@RestController
@RequestMapping("/voyage")
public class VoyageController {
	 @Autowired
	    private VoyageService voyageService;
	  	
	    @PostMapping("/add")
	    public Voyage saveVoyage(@RequestBody  Voyage voyage){
	    	
	    		voyageService.saveVoyage(voyage);
	    		return  voyage;
	    	
	    	
	    	
	    }
	    
	    @GetMapping("/voyages")  
	    private List<Voyage> getAVoyages()   
	    {  
	    return  voyageService.getAllVoyages();  
   
	   }
	    
	    @RequestMapping(value = "/find", method = RequestMethod.GET)
	   public List<Voyage> getByObjectDeVoyageAndDestination(@Param(value = "objectDeVoyage") String objectDeVoyage,@Param(value="destination") String destination) {
	    	
	    	return  voyageService.findByOD(objectDeVoyage,destination);
		}
	    
	   
	  //creating a delete mapping that deletes a specified book  
	    @DeleteMapping("/{id}")  
	    private void deleteBook(@PathVariable("id") int id)   
	    {  
	    	voyageService.delete(id);  
	    }  
	    
	    
	    @PutMapping("/update/{id}")
	    public Voyage editVoyage(@PathVariable("id") int id ,@RequestBody  Voyage voyage){
	    	
	    		voyageService.editVoyage(id,voyage);
	    		return  voyage;
	    	
	    	
	    	
	    }
	    @GetMapping("/{id}")
	    
	    private ResponseEntity<Voyage> getVoyageById(@PathVariable("id") int id)   
	    {  
	    	Voyage foundVoyage = voyageService.findById(id);
	    	 if (foundVoyage == null) {
	    	        return ResponseEntity.notFound().build();
	    	    } else {
	    	        return ResponseEntity.ok(foundVoyage);
	    	    }
	    } 
	   
	}


@Data
class Form{
    private String objectDeVoyage;
    private String destination;
	public String getObjectDeVoyage() {
		return objectDeVoyage;
	}
	public void setObjectDeVoyage(String objectDeVoyage) {
		this.objectDeVoyage = objectDeVoyage;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}

}


