package com.example.demo.controller;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.MailService;

import lombok.Data;


@RestController
public class SendMailController {
	 @Autowired
	    private MailService mailService;
	 @PostMapping("/sendmail")
	    public void sendEmail(@RequestBody  EmailForm emailForm) throws MessagingException{
	        mailService.sendEmail(emailForm.getMailTo(), emailForm.getMailSubject(),emailForm.getMailContent(), emailForm.getMailType());
	    }
}
@Data
class EmailForm{
	private String mailTo;
	private String mailSubject;
	private String mailContent;
	private String mailType;
	public String getMailTo() {
		return mailTo;
	}
	public void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}
	public String getMailSubject() {
		return mailSubject;
	}
	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}
	public String getMailContent() {
		return mailContent;
	}
	public void setMailContent(String mailContent) {
		this.mailContent = mailContent;
	}
	public String getMailType() {
		return mailType;
	}
	public void setMailType(String mailType) {
		this.mailType = mailType;
	}
	

    
}
